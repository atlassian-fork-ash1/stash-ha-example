# Stash High Availability #

This repository contains scripts that can be used to quickly set up
a high available testing environment for Stash. This environment consists
of 2 nodes. The environment consists of

* All Stash requirements: openjdk 1.7.0, git 1.8.3.4
* [DRBD](http://www.drbd.org/) managed disk partition for replicated filesystem with a 
  [GFS2](https://access.redhat.com/site/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Global_File_System_2/) clustered filesystem on /data.
* RedHat ClusterManager for managing the GFS2 filesystem.
* Corosync and Pacemaker for managing clustered/high available resources such as Stash.


# Installation instructions (on MacOSX) #

To build and run the virtualbox / vagrant images, you'll have to install a bunch of 
applications. 

### XCode ###
- Install XCode - search for XCode in the app store
- Run XCode from your applications folder and accept the license agreements
- Install Command Line Tools. 
  - In XCode, open XCode menu and select Preferences
  - Choose the Downloads tab and click the Install button next to the Command Line Tools item.

### Homebrew ###

    ruby < (curl -dsSkl raw.github.com/mxcl/homebrew/go)
    brew doctor

### VirtualBox ###

    cd /tmp
    wget http://download.virtualbox.org/virtualbox/4.3.2/VirtualBox-4.3.2-90405-OSX.dmg
    open VirtualBox*.dmg

### Packer ###

    brew tap homebrew/binary
    brew install packer

### Vagrant ###

    cd /tmp
    wget http://files.vagrantup.com/packages/a40522f5fabccb9ddabad03d836e120ff5d14093/Vagrant-1.3.5.dmg
    open Vagrant-1.3.5.dmg



# Build the VirtualBox base image #

We'll use vagrant to start up and provision 2 Stash cluster nodes. In theory more nodes could be added 
to the cluster, but the replicating filesystem that we're using (DRBD) only supports 2 nodes with the
simple configuration we're using. 

Each of the nodes starts a _base_ virtualbox image. We'll use packer to create this base image to ensure 
the image has the partitions we need and all the software components we'll need for our cluster. By 
provisioning this in the base image, vagrant starts up more quickly, saving us time in the following steps.

#### Clone the stash-ha repository. 

Well, you're reading this, so presumably you've already done that.

    git clone ssh://git@stash.dev.internal.atlassian.com/stash/stash-ha.git

#### Create the virtualbox base image

    cd packer
    packer build centos64.json

Packer has now generated a packer_virtualbox_virtualbox.vbox image. Let's move it to the directory
used by vagrant.

    mv packer_virtualbox_virtualbox.vbox ../boxes/centos64.box

If you've rebuilt the packer box, you'll need to wipe the current vagrant box to ensure the new box
is picked up.

    rm -Rf ~/.vagrant.d/boxes/stashNode

# Start up the cluster

#### Configure the Stash version to use

Out of the box, Stash 2.9.2 will be downloaded and installed when the vagrant boxes start up. You can
customize this by editing the STASH_VERSION variable in `vagrant/scripts/provision-stash.sh`

#### Start up the vagrant boxes

    cd ../vagrant
    vagrant up

Starting up and provisioning both nodes can take a couple of minutes, so grab a coffee and sit tight.
After both nodes have started up, it will be another minute before Stash has started up.

You can now access Stash on http://192.168.56.99:7990/stash. This is a 'high available' IP, that is
managed by the cluster stack. It will move to the node that Stash is currently running on.

#### SSH into the nodes
    vagrant ssh node1

#### View cluster status
    sudo pcs status


# Simulate a failover

Figure out what node Stash is running on. Most likely, it'll be node2

    [~]$ vagrant ssh node2

    [~]$ sudo pcs status
    Cluster name: atlascluster
    Last updated: Thu Nov 21 16:04:26 2013
    Last change: Thu Nov 21 14:43:35 2013 via cibadmin on stash2.cluster
    Stack: cman
    Current DC: stash1.cluster - partition with quorum
    Version: 1.1.10-1.el6_4.4-368c726
    2 Nodes configured
    2 Resources configured
    
    
    Online: [ stash1.cluster stash2.cluster ]
    
    Full list of resources:
    
    ha_ip	  (ocf::heartbeat:IPaddr2):	Started stash2.cluster 
    stash_res	  (ocf::heartbeat:stash):	Started stash2.cluster 
    
    [~]$ ps aux | grep ootstrap
    stash     3573  6.2 40.0 1968096 770512 ?      Sl   14:43   4:42 /usr/bin/java -Djava.util.logging.config.file=/opt/stash/current/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -XX:MaxPermSize=256m -Xms512m -Xmx768m -Djava.awt.headless=true -Dfile.encoding=UTF-8 -Datlassian.standalone=STASH -Dorg.apache.jasper.runtime.BodyContentImpl.LIMIT_BUFFER=true -Dmail.mime.decodeparameters=true -Dorg.apache.catalina.connector.Response.ENFORCE_ENCODING_IN_GET_WRITER=false -Djava.library.path=/opt/stash/current/lib/native:/data/stash/home/lib/native -Dstash.home=/data/stash/home -Djava.endorsed.dirs=/opt/stash/current/endorsed -classpath /opt/stash/current/bin/bootstrap.jar:/opt/stash/current/bin/tomcat-juli.jar -Dcatalina.base=/opt/stash/current -Dcatalina.home=/opt/stash/current -Djava.io.tmpdir=/opt/stash/current/temp org.apache.catalina.startup.Bootstrap start
    [~]$ sudo kill 3573

Now wait a minute or two for the cluster to detect that Stash is down and start up Stash on the other node. 

Make sure to cleanup the stash_res resource afterwards. If we don't, the cluster will consider stash2.cluster to be in a bad state and won't failover to it. 

    [~]$ sudo pcs resource cleanup stash_res 

# What's where?
<style>
table {
      font-family:Arial, Helvetica, sans-serif;
      color:#666;
      font-size:12px;
      text-shadow: 1px 1px 0px #fff;
      background:#eaebec;
      margin:20px;
      border:#ccc 1px solid;

      -moz-border-radius:3px;
      -webkit-border-radius:3px;
      border-radius:3px;

      -moz-box-shadow: 0 1px 2px #d1d1d1;
      -webkit-box-shadow: 0 1px 2px #d1d1d1;
      box-shadow: 0 1px 2px #d1d1d1;
}
table th {
      padding:21px 25px 22px 25px;
      border-top:1px solid #fafafa;
      border-bottom:1px solid #e0e0e0;

      background: #ededed;
      background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
      background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
table th:first-child {
      text-align: left;
      padding-left:20px;
}
table tr:first-child th:first-child {
      -moz-border-radius-topleft:3px;
      -webkit-border-top-left-radius:3px;
      border-top-left-radius:3px;
}
table tr:first-child th:last-child {
      -moz-border-radius-topright:3px;
      -webkit-border-top-right-radius:3px;
      border-top-right-radius:3px;
}
table tr {
      text-align: center;
      padding-left:20px;
}
table td:first-child {
      text-align: left;
      padding-left:20px;
      border-left: 0;
}
table td {
      padding:18px;
      border-top: 1px solid #ffffff;
      border-bottom:1px solid #e0e0e0;
      border-left: 1px solid #e0e0e0;

      background: #fafafa;
      background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
      background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
}
table tr.even td {
      background: #f6f6f6;
      background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
      background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
}
table tr:last-child td {
      border-bottom:0;
}
table tr:last-child td:first-child {
      -moz-border-radius-bottomleft:3px;
      -webkit-border-bottom-left-radius:3px;
      border-bottom-left-radius:3px;
}
table tr:last-child td:last-child {
      -moz-border-radius-bottomright:3px;
      -webkit-border-bottom-right-radius:3px;
      border-bottom-right-radius:3px;
}
table tr:hover td {
      background: #f2f2f2;
      background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
      background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);  
}
</style>

<table>
 <thead><tr><th>Path</th><th>Description</th></tr></thead>
 <tbody>
  <TR><td>boxes</td><td>The base virtualbox image is written here by Packer and is used by vagrant</td></tr>
  <tr><td>iso</td><td>Packer stores the iso files it downloaded here</td></tr>
  <tr><td>packer</td><td>Contains packer configuration and scripts</td></tr>
  <tr><td>vagrant</td><td>Contains vagrant configuration and scripts</td></tr>
 </tbody>
</table>